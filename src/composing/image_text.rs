use image::{
    RgbaImage,
    Rgba
};
use rusttype::{Font, Scale, point, PositionedGlyph};
use imageproc::{drawing::{Canvas}, pixelops::weighted_sum};
use super::{utils::ImageManipulator};
use textwrap::Wrapper;

pub struct ImageText;

impl ImageText {

    fn draw_text_stat(canvas: &mut RgbaImage, color: Rgba<u8>, position: (i32, i32), font_size: u16, font: &Font, text: &str) {
        let scale = Scale::uniform(font_size as f32);
        let v_metrics = font.v_metrics(scale);
        let offset = point(0.0, v_metrics.ascent);
    
        let glyphs: Vec<PositionedGlyph<'_>> = font.layout(text, scale, offset).collect();
    
        for g in glyphs {
            if let Some(bb) = g.pixel_bounding_box() {
                g.draw(|gx, gy, gv| {
                    let gx = gx as i32 + bb.min.x;
                    let gy = gy as i32 + bb.min.y;
    
                    let image_x = gx + position.0;
                    let image_y = gy + position.1;
    
                    let image_width = canvas.width() as i32;
                    let image_height = canvas.height() as i32;
    
                    if image_x >= 0 && image_x < image_width && image_y >= 0 && image_y < image_height {
                        let pixel = *(canvas.get_pixel(image_x as u32, image_y as u32));
                        let weighted_color = weighted_sum(pixel, color, 1.0 - gv, gv);
                        canvas.draw_pixel(image_x as u32, image_y as u32, weighted_color);
                    }
                })
            }
        }
    }

    fn get_text_size(font: &Font, font_size: u16, text: String) -> Option<(f32, f32)> {
        let scale: Scale = Scale::uniform(font_size as f32); // meaning max_height will correspond to font_size

        let mut min_x: Option<i32> = None;
        let mut max_x: Option<i32> = None;
        let mut min_y: Option<i32> = None;
        let mut max_y: Option<i32> = None;
        for glyph in font.layout(&text, scale, point(0.0, 0.0)) {
            let pixel_box = glyph.pixel_bounding_box();
            if let Some(pixel_box) = pixel_box {
                if min_x.is_none() || min_x.unwrap() > pixel_box.min.x {
                    min_x = Some(pixel_box.min.x);
                }
                if max_x.is_none() || max_x.unwrap() < pixel_box.max.x {
                    max_x = Some(pixel_box.max.x);
                }
                if min_y.is_none() || min_y.unwrap() > pixel_box.min.y {
                    min_y = Some(pixel_box.min.y);
                }
                if max_y.is_none() || max_y.unwrap() < pixel_box.max.y {
                    max_y = Some(pixel_box.max.y);
                }
            }
        }

        if min_x.is_none() || max_x.is_none() || min_y.is_none() || max_y.is_none() {
            None
        } else {
            Some(((max_x.unwrap() - min_x.unwrap()) as f32, (max_y.unwrap() - min_y.unwrap()) as f32))
        }
    }


    pub fn generate_canvas(
        message: String,
        font: &Font,
        canvas_color: Rgba<u8>,
        font_color: Rgba<u8>,
        aspect_ratio: f32
    ) -> RgbaImage {

        let font_size = 200;
        let optimal_wrap_lines = Self::get_optimal_wrap_for_aspect_ratio(
            &message[..],
            font, 
            aspect_ratio,
            font_size
        );

        let canvas_size = 2000;
        let canvas_size = (canvas_size * 2, canvas_size);

        let mut canvas = RgbaImage::from_pixel(canvas_size.0, canvas_size.1, canvas_color);
        let sizes = 
            optimal_wrap_lines
            .iter()
            .map(|el| Self::get_text_size(font, font_size, el.clone()))
            .collect::<Vec<Option<(f32, f32)>>>();
        let vertical_cut: f32 = sizes
            .iter()
            .take(sizes.len() / 2)
            .filter(|opt| opt.is_some())
            .map(|opt| opt.as_ref().unwrap().1)
            .sum();
        let mut cur_y: f32 = (canvas_size.1 as f32 / 2.0) - vertical_cut;
        let horizontal_halfpoint = canvas_size.0 as f32 / 2.0;
        let text_elements = optimal_wrap_lines.into_iter().zip(sizes.into_iter());
        for text_element in text_elements {
            if let Some(text_size) = text_element.1 {
                let text = text_element.0;
                let cur_x = horizontal_halfpoint - text_size.0 / 2.0;

                Self::draw_text_stat(&mut canvas, font_color, (cur_x as i32, cur_y as i32), font_size, font, &text[..]);

                cur_y += text_size.1;
            }
        }

        let canvas = ImageManipulator::trim_edges(&canvas, canvas_color);

        let real_aspect_ratio = canvas.dimensions().0 as f32 / canvas.dimensions().1 as f32;
        let mut new_width: u32 = canvas.dimensions().0;
        let mut new_height: u32 = canvas.dimensions().1;
        if real_aspect_ratio > aspect_ratio {
            // need to add height
            new_height = (new_width as f32 / aspect_ratio) as u32;
        } else {
            // need to add width
            new_width = (new_height as f32 * aspect_ratio) as u32;
        }
        let canvas = ImageManipulator::fit_image(&canvas, canvas_color, (new_width, new_height));

        canvas

    }

    fn get_optimal_wrap_for_aspect_ratio(
        query: &str,
        font: &Font,
        aspect_ratio: f32,
        font_size: u16
    ) -> Vec<String> {

        let query_length = query.len();
        let mut closest_wrap_lines: Option<Vec<String>> = None;
        let mut closest_wrap_delta: Option<f32> = None;
        for i in 1 .. query_length + 1 {
            let lines = Wrapper::new(i).break_words(false).wrap(&query).into_iter().map(|s| String::from(s)).collect::<Vec<String>>();
            let mut width = 0.0;
            let mut height = 0.0;
            for line in &lines {
                let size = Self::get_text_size(font, font_size, line.clone());
                if let Some(size) = size {
                    if size.0 > width {
                        width = size.0;
                    }
                    height += size.1;
                }
            }

            let ratio = width / height;
            let delta = (ratio - aspect_ratio).abs();
            if closest_wrap_delta.is_none() || closest_wrap_delta.unwrap() > delta {
                closest_wrap_delta = Some(delta);
                closest_wrap_lines = Some(lines);
            }
        }

        if let Some(closest_wrap_lines) = closest_wrap_lines {
            closest_wrap_lines
        } else {
            vec![String::from(query)]
        }

    }

}
