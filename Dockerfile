FROM docker.io/rust:1.60-alpine AS build
WORKDIR /build
RUN apk add --no-cache musl-dev
COPY . .
RUN cargo build --release

FROM docker.io/alpine:3 AS runtime
ENV ROCKET_ADDRESS=0.0.0.0
ENTRYPOINT [ "/bin/sb-composer" ]
COPY --from=build /build/target/release/sb-composer /bin/sb-composer
