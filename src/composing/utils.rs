use image::{RgbaImage, Rgba, imageops};
use imageproc::{
    geometric_transformations::{
        Interpolation,
        warp, Projection
    }
};
use imageops::FilterType;
pub struct ImageManipulator;

impl ImageManipulator {

    pub fn trim_edges(image: &RgbaImage, filtered_color: Rgba<u8>) -> RgbaImage {


        let original_width = image.dimensions().0;
        let original_height = image.dimensions().1;

        // scan all lines from the top, stop and save index when line not matching filtered_color
        let mut trim_top: u32 = 0;
        for y in 0 .. original_height {
            let mut all_trimmable = true;
            for x in 0 .. original_width {
                let pixel = image.get_pixel(x, y);
                if pixel.0 != filtered_color.0 || pixel.0[3] == 0 {
                    all_trimmable = false;
                    break;
                }
            }
            if !all_trimmable && y > 0 {
                trim_top = y - 1;
                break;
            }
        }

        // scan all lines from the bottom, stop and save index when line not matching filtered_color
        let mut trim_bottom: u32 = 0;
        for y in 0 .. original_height {
            let mut all_trimmable = true;
            for x in 0 .. original_width {
                let pixel = image.get_pixel(x, original_height - 1 - y);
                if pixel.0 != filtered_color.0 || pixel.0[3] == 0 {
                    all_trimmable = false;
                    break;
                }
            }
            if !all_trimmable && y > 0 {
                trim_bottom = y - 1;
                break;
            }
        }

        // scan all lines from the left, stop and save index when line not matching filtered_color
        let mut trim_left: u32 = 0;
        for x in 0 .. original_width {
            let mut all_trimmable = true;
            for y in 0 .. original_height {
                let pixel = image.get_pixel(x, y);
                if pixel.0 != filtered_color.0 || pixel.0[3] == 0 {
                    all_trimmable = false;
                    break;
                }
            }
            if !all_trimmable && x > 0 {
                trim_left = x - 1;
                break;
            }
        }

        // scan all lines from the right, stop and save index when line not matching filtered_color
        let mut trim_right: u32 = 0;
        for x in 0 .. original_width {
            let mut all_trimmable = true;
            for y in 0 .. original_height {
                let pixel = image.get_pixel(original_width - 1 - x, y);
                if pixel.0 != filtered_color.0 || pixel.0[3] == 0 {
                    all_trimmable = false;
                    break;
                }
            }
            if !all_trimmable && x > 0 {
                trim_right = x - 1;
                break;
            }
        }

        let crop = imageops::crop_imm(
            image,
            trim_left,
            trim_top,
            original_width - trim_left - trim_right,
            original_height - trim_top - trim_bottom
        );
        let crop = crop.to_image();

        crop

    }

    pub fn fit_image(fitted_image: &RgbaImage, fill_color: Rgba<u8>, target_size: (u32, u32)) -> RgbaImage {
        let mut new_image = RgbaImage::from_pixel(target_size.0, target_size.1, fill_color);
        let original_dimensions = fitted_image.dimensions();
        //println!("TS: {:?}, OD: {:?}", target_size, original_dimensions);
        if original_dimensions.0 > target_size.0 || original_dimensions.1 > target_size.1 {
            let resized_image = {
                let scale_factor =  target_size.1 as f32 / original_dimensions.1 as f32;
                let new_width = original_dimensions.0 as f32 * scale_factor;
                if new_width > target_size.0 as f32 {
                    let scale_factor = target_size.0 as f32 / original_dimensions.0 as f32;
                    let new_height = original_dimensions.1 as f32 * scale_factor;
                    imageops::resize(fitted_image, target_size.0, new_height as u32, FilterType::CatmullRom)
                }    else {
                    imageops::resize(fitted_image, new_width as u32, target_size.1, FilterType::CatmullRom)
                }
            };
            Self::fit_image(&resized_image, fill_color, target_size)
        } else {
            let new_x = (target_size.0 - original_dimensions.0) / 2;
            let new_y = (target_size.1 - original_dimensions.1) / 2;
            imageops::overlay(&mut new_image, fitted_image, new_x, new_y);
            new_image
        }
    }

    pub fn add_margin(original_image: &RgbaImage, margin_color: Rgba<u8>, horizontal: u32, vertical: u32) -> RgbaImage {
        let original_dimensions = original_image.dimensions();
        let new_dimensions = (original_dimensions.0 + 2 * horizontal, original_dimensions.1 + 2 * vertical);
        Self::fit_image(original_image, margin_color, new_dimensions)
    }

    pub fn compose_image(top_image: &RgbaImage, bottom_image: &RgbaImage, control_points: [(f32, f32); 4]) -> RgbaImage {

        let transparent_color = Rgba::from([0, 0, 0, 0]);

        let top_dimensions = top_image.dimensions();
        let bottom_dimensions = bottom_image.dimensions();
        let upfilled_bottom_image = Self::fit_image(&bottom_image, transparent_color, top_dimensions);

        let upfilled_left = (top_dimensions.0 / 2 - bottom_dimensions.0 / 2) as f32;
        let upfilled_right = (top_dimensions.0 / 2 + bottom_dimensions.0 / 2) as f32;
        let upfilled_top = (top_dimensions.1 / 2 - bottom_dimensions.1 / 2) as f32;
        let upfilled_bottom = (top_dimensions.1 / 2 + bottom_dimensions.1 / 2) as f32;

        let source_points = [
            (upfilled_left, upfilled_top),
            (upfilled_right, upfilled_top),
            (upfilled_right, upfilled_bottom),
            (upfilled_left, upfilled_bottom)
        ];
        let destination_points = control_points;

        let projection = Projection::from_control_points(source_points, destination_points);
        if projection.is_none() {
            eprintln!("Unable to calculate projection matrix");
            panic!();
        }
        let projection = projection.unwrap();

        let warped_bottom = warp(
            &upfilled_bottom_image, 
            &projection,
            Interpolation::Bicubic,
            transparent_color
        );

        let mut composite_image = RgbaImage::from_pixel(
            top_dimensions.0,
            top_dimensions.1,
            transparent_color
        );
        imageops::overlay(&mut composite_image, &warped_bottom, 0, 0);
        imageops::overlay(&mut composite_image, top_image, 0, 0);

        composite_image

    }

    pub fn encode_webp(input_image: &[u8], width: u32, height: u32) -> Option<Vec<u8>> {
        let encoder = webp::Encoder::from_rgba(input_image, width, height);
        let encoded_memory = encoder.encode(50.0);
        return Some(encoded_memory.to_vec());
    }

}
