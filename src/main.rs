pub mod composing;

use composing::composition::Composition;
use composing::utils::ImageManipulator;
use image::EncodableLayout;
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::{Json, Value, json};
use rocket::serde::Deserialize;

#[macro_use]
extern crate rocket;

#[derive(Deserialize)]
struct ComposeRequest<'a> {
    source_png_base64: &'a str,
    canvas_control_point_a_x: f32,
    canvas_control_point_a_y: f32,
    canvas_control_point_b_x: f32,
    canvas_control_point_b_y: f32,
    canvas_control_point_c_x: f32,
    canvas_control_point_c_y: f32,
    canvas_control_point_d_x: f32,
    canvas_control_point_d_y: f32,
    canvas_padding_width: u32,
    canvas_padding_height: u32,
    canvas_color_red: u8,
    canvas_color_green: u8,
    canvas_color_blue: u8,
    canvas_color_alpha: u8,
    font_file_base64: &'a str,
    font_color_red: u8,
    font_color_green: u8,
    font_color_blue: u8,
    font_color_alpha: u8,
    text: &'a str
}

#[post("/compose", data = "<request>")]
fn compose(request: Json<ComposeRequest<'_>>) -> Value {
    let composition = Composition::new(
        base64::decode(request.source_png_base64).expect("Unable to decode PNG"),
        [
            (request.canvas_control_point_a_x, request.canvas_control_point_a_y),
            (request.canvas_control_point_b_x, request.canvas_control_point_b_y),
            (request.canvas_control_point_c_x, request.canvas_control_point_c_y),
            (request.canvas_control_point_d_x, request.canvas_control_point_d_y)
        ],
        (request.canvas_padding_width, request.canvas_padding_height),
        [
            request.canvas_color_red,
            request.canvas_color_green,
            request.canvas_color_blue,
            request.canvas_color_alpha
        ],
        base64::decode(request.font_file_base64).expect("Unable to decode Font"),
        [
            request.font_color_red,
            request.font_color_green,
            request.font_color_blue,
            request.font_color_alpha
        ]
    );
    let image = composition.compose_text(String::from(request.text));
    let webp = ImageManipulator::encode_webp(image.as_bytes(), image.dimensions().0, image.dimensions().1);
    if let Some(webp) = webp {
        json!({
            "encoded_webp": base64::encode(webp),
            "error": ""
        })
    } else {
        json!({
            "encoded_webp": "",
            "error": "No WebP generated"
        })
    }
}

#[get("/ready")]
fn ready() -> status::Custom<&'static str>{
    status::Custom(Status::Ok, ":)")
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![compose, ready])
}
