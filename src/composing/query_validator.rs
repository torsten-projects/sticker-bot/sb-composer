
use image::Rgba;

pub struct ValidationError {
    pub color: Rgba<u8>,
    pub message: String
}

impl ValidationError {

    pub fn red(message: &str) -> ValidationError {
        ValidationError {
            color: Rgba::from([255, 0, 0, 255]),
            message: String::from(message)
        }
    }

}

pub struct QueryValidator;

impl QueryValidator {

    pub fn validate(query: String) -> Result<(), ValidationError> {

        let words = query.split(&[' ', '\n'][..]).collect::<Vec<_>>();
        let word_count = words.len();

        if word_count > 75 {
            return Err(ValidationError::red("Too many words"));
        }
        
        if word_count == 0 || words.iter().all(|el| el.len() == 0) {
            return Err(ValidationError::red("Empty query"));
        }

        Ok(())
    }

}