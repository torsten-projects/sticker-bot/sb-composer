use image::{Rgba, ImageBuffer, RgbaImage, imageops::{FilterType, resize}};
use super::{utils::ImageManipulator, image_text::ImageText};
use rusttype::Font;

#[derive(Clone)]
pub struct Composition {
    source_image: ImageBuffer<Rgba<u8>, Vec<u8>>,
    canvas_size: (u32, u32),
    canvas_control_points: [(f32, f32); 4],
    canvas_padding: (u32, u32),
    canvas_color: Rgba<u8>,
    font_file: Vec<u8>,
    font_color: Rgba<u8>
}

impl Composition {

    pub fn new(
        source_file: Vec<u8>,
        canvas_control_points: [(f32, f32); 4],
        canvas_padding: (u32, u32),
        canvas_color: [u8; 4],
        font_file: Vec<u8>,
        font_color: [u8; 4]
    ) -> Composition {
        let source_image = image::load_from_memory(&source_file[..]);
        if source_image.is_err() {
            eprintln!("Unable to open input image for composition");
            panic!();
        }
        let source_image = source_image.unwrap();
        let source_image = source_image.as_rgba8();
        if source_image.is_none() {
            eprintln!("Unable to parse input image as RGBA8");
            panic!();
        }
        let source_image = source_image.unwrap().clone();

        let canvas_size = Self::calculate_canvas_size(canvas_control_points);

        Composition {
            source_image,
            canvas_size,
            canvas_control_points,
            canvas_padding,
            canvas_color: Rgba::from(canvas_color),
            font_file,
            font_color: Rgba::from(font_color)
        }
    }

    pub fn compose_text(&self, query: String) -> RgbaImage {
        let canvas = self.get_canvas_for_text(query);
        self.compose(canvas)
    }

    pub fn compose_image(&self, image: RgbaImage) -> RgbaImage {
        let image = ImageManipulator::trim_edges(&image, self.canvas_color);
        ImageManipulator::fit_image(&image, self.canvas_color, self.get_canvas_size());
        let canvas = self.get_canvas_for_image(&image);
        self.compose(canvas)
    }

    fn compose(&self, canvas: RgbaImage) -> RgbaImage {
        let template_image = self.get_current_template();
        let composite_image = ImageManipulator::compose_image(
            &template_image, 
            &canvas, 
            self.canvas_control_points
        );

        composite_image
    }

    fn get_current_template(&self) -> RgbaImage {
        self.source_image.clone()
    }

    fn get_canvas_for_image(&self, image: &RgbaImage) -> RgbaImage {
        let canvas_size = (
            self.canvas_size.0 - self.canvas_padding.0 * 2,
            self.canvas_size.1 - self.canvas_padding.1 * 2
        );
        let image = ImageManipulator::trim_edges(image, self.canvas_color);
        ImageManipulator::fit_image(&image, self.canvas_color, canvas_size)
    }

    fn get_canvas_for_text(&self, query: String) -> RgbaImage {
        let font = Font::try_from_vec((&self.font_file).clone());
        if font.is_none() {
            eprintln!("Unable to open font file");
            panic!();
        }
        let font = font.unwrap();

        let canvas_size = (
            self.canvas_size.0 - self.canvas_padding.0 * 2,
            self.canvas_size.1 - self.canvas_padding.1 * 2
        );

        let aspect_ratio = canvas_size.0 as f32 / canvas_size.1 as f32;
        let text_image = ImageText::generate_canvas(query, &font, self.canvas_color, self.font_color, aspect_ratio);
        let text_image = resize(&text_image, canvas_size.0, canvas_size.1, FilterType::CatmullRom);
        let canvas_image = ImageManipulator::add_margin(&text_image, self.canvas_color, self.canvas_padding.0, self.canvas_padding.1);
        canvas_image
    }

    fn calculate_canvas_size(points: [(f32, f32); 4]) -> (u32, u32) {
        let left = Self::point_distance(points[0], points[3]);
        let right = Self::point_distance(points[1], points[2]);
        let height = if left > right { left } else { right };
        let top = Self::point_distance(points[0], points[1]);
        let bottom = Self::point_distance(points[2], points[3]);
        let width = if top > bottom { top } else { bottom };
        (width.ceil() as u32, height.ceil() as u32)
    }

    fn point_distance(a: (f32, f32), b: (f32, f32)) -> f32 {
        ((a.0 - b.0).abs().powi(2) + (a.1 - b.1).abs().powi(2)).sqrt()
    }

    fn get_canvas_size(&self) -> (u32, u32) {
        (
            self.canvas_size.0 - self.canvas_padding.0 * 2,
            self.canvas_size.1 - self.canvas_padding.1 * 2
        )
    }

}
