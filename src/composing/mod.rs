pub mod image_text;
pub mod query_validator;
pub mod utils;
pub mod composition;

use composition::Composition;

#[derive(Clone)]
pub struct CompositionRepository {
    pub repo: Vec<Composition>
}

impl CompositionRepository {

    pub fn new() -> CompositionRepository {

        CompositionRepository {
            repo: vec![]
        }

    }

    pub fn register_composition(mut self, composition: Composition) -> Self {
        self.repo.push(composition.clone());
        self
    }

}